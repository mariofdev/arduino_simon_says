int blueLedPin = 5;
int redLedPin = 9;
int greenLedPin = 6;

int buzzPin = 11;

int buttonPinRed = 2;
int buttonPinGreen = 3;
int buttonPinBlue = 4;

int redButtonRead;
int greenButtonRead;
int blueButtonRead;

int colors[] = {blueLedPin, redLedPin, greenLedPin};
int counter = 0;
int colorsList[100];

int delayTime = 1800;

void setup() {
  randomSeed(analogRead(0));

  Serial.begin(9600);

  pinMode(buzzPin, OUTPUT);

  pinMode(redLedPin, OUTPUT);
  pinMode(greenLedPin, OUTPUT);
  pinMode(blueLedPin, OUTPUT);

  pinMode(buttonPinRed, INPUT);
  pinMode(buttonPinGreen, INPUT);
  pinMode(buttonPinBlue, INPUT);
}

void loop() {
  int randomNumber = random(3);

  colorsList[counter] = colors[randomNumber];
  counter++;

  for(int i = 0; i < counter; i++){
    analogWrite(colorsList[i], 20);
    analogWrite(buzzPin, 1);

    delay(delayTime);

    digitalWrite(buzzPin, 0);
    analogWrite(colorsList[i], 0);

    delay(180);
  }
  
  delayTime = (delayTime > 600) ? delayTime - 150 : delayTime;

  for(int i = 0; i < counter; i++){
    int selectedColor = getSelectedColor();
    int expectedColor = colorsList[i];

    if (selectedColor != expectedColor) {
      loseMatch();
      break;
    }

    analogWrite(selectedColor, 20);
    analogWrite(buzzPin, 2);

    delay(500);

    analogWrite(selectedColor, 0);
    analogWrite(buzzPin, 0);
  }

  delay(1000);

}

int getSelectedColor() {
  delay(200);

  redButtonRead = digitalRead(buttonPinRed);
  greenButtonRead = digitalRead(buttonPinGreen);
  blueButtonRead = digitalRead(buttonPinBlue);

  while(redButtonRead == 1 && greenButtonRead == 1 && blueButtonRead == 1){
    redButtonRead = digitalRead(buttonPinRed);
    greenButtonRead = digitalRead(buttonPinGreen);
    blueButtonRead = digitalRead(buttonPinBlue);
    delay(200);
  }

  if(redButtonRead == 0){ 
    return redLedPin;
  }
  else if(greenButtonRead == 0){
    return greenLedPin;

  }
  else if(blueButtonRead == 0){
    return blueLedPin;

  }
}

void loseMatch() {
  analogWrite(blueLedPin, 100);
  analogWrite(greenLedPin, 100);
  analogWrite(redLedPin, 100);

  analogWrite(buzzPin, 2);

  delay(3000);

  analogWrite(buzzPin, 0);

  analogWrite(blueLedPin, 0);
  analogWrite(greenLedPin, 0);
  analogWrite(redLedPin, 0);

  memset(colorsList, 0, sizeof(colorsList));
  counter = 0;
  delayTime = 1800;
}
