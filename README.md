# Arduino Simon Says

A simple Arduino Simon Says game.

## Overview

This project is a simplified version of the classic "Simon Says" game. As you advance in the game and correctly repeat the sequence of colors, the speed of the game increases, just like in the original game. However, if you make a mistake, the game will signal this by turning on all three LEDs for 3 seconds, and then the game restarts.

For a quick visual demonstration of how the game works, you can watch this short video on YouTube: [Watch Demo](https://youtube.com/shorts/4NH9cDMiPsk).

## Components Used

The circuit for this project was created using the following components:

- 3 x 200Ω resistors
- 3 x 10KΩ resistors
- 1 green LED
- 1 blue LED
- 1 red LED
- 1 active buzzer
- 16 male-to-male jumper cables
- Arduino Uno
